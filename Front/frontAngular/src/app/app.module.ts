import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { Routes, RouterModule } from '@angular/router';
import { FilmsComponent } from './films/films.component';
import { ClientsComponent } from './clients/clients.component';
import { AddFilmsComponent } from './add-films/add-films.component';
import { AddClientsComponent } from './add-clients/add-clients.component';
import { ReservationsComponent } from './reservations/reservations.component';
import { FilmsService } from './service/films.service';

const appRoutes: Routes = [
  { path: 'films', component:  FilmsComponent},
  { path: 'clients', component: ClientsComponent},
  { path: 'addFilms', component: AddFilmsComponent},
  { path: 'addClients', component: AddClientsComponent},
  { path: 'reservations', component: ReservationsComponent},
  { path: '', component: AcceuilComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    FilmsComponent,
    ClientsComponent,
    AddFilmsComponent,
    AddClientsComponent,
    ReservationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
   // HttpClientModule
  ],
  providers: [
    FilmsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
