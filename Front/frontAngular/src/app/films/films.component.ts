import { Component, OnInit } from '@angular/core';
import { FilmsService } from '../service/films.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit{

  Listefilms : any[];

  
  constructor(private fService : FilmsService) { }
  
  ngOnInit(){
    this.fService.subj.subscribe(
      (data) => {
        this.Listefilms = data;
      }
    );
    this.fService.emitElements();
  }
  

}
